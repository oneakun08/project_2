package com.harjayreza.basicwidget1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.harjayreza.basicwidget1.adapter.KotaAdapter;

import java.util.ArrayList;
import java.util.Arrays;

public class Main5Activity extends AppCompatActivity {

    Spinner spPropinsi, spKota;
    TextView tvHasil;

    String[][] arKota = {{"Jakarta Barat", "Jakarta Pusat", "Jakarta Selatan", "Jakarta Timur", "jakarta Utara"}
            , {"Bandung", "Cirebon", "Bekasi"}, {"semarang", "Magelang", "Surakarta"},
            {"Surabaya", "Malang", "Blitar"}, {"Denpasar"}};
    ArrayList<String> listKota = new ArrayList<>();
    KotaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);

        spPropinsi = findViewById(R.id.spinnerPropinsi);
        spKota = findViewById(R.id.spinnerKota);
        tvHasil = findViewById(R.id.textViewHasil);
        //        adapter=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listKota);
        adapter = new KotaAdapter(this, listKota);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spKota.setAdapter(adapter);

        spPropinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                listKota.clear();
                listKota.addAll(Arrays.asList(arKota[pos]));
                adapter.setmPropinsi((String) spPropinsi.getItemAtPosition(pos));
                adapter.notifyDataSetChanged();
                spKota.setSelection(0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        findViewById(R.id.buttonOK1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doClick();
            }
        });
    }

    private void doClick() {
//        tvHasil.setText("Wilayah Propinsi "+ spPropinsi.getSelectedItem().toString()
//        +" Kota "+spKota.getSelectedItem().toString());
        StringBuilder builder = new StringBuilder();
        builder.append("Wilayah Propinsi");
        builder.append(spPropinsi.getSelectedItem().toString());
        builder.append("Kota");
        builder.append(spKota.getSelectedItem().toString());
        builder.append("\n\n\n");

        builder.append("Kota yang Tidak Dipilih : \n\n");
        String[] arPropinsi = getResources().getStringArray(R.array.propinsi);
        int posProp = spPropinsi.getSelectedItemPosition();
        int posKota = spKota.getSelectedItemPosition();

        for (int i = 0; i < arPropinsi.length; i++) {
            builder.append(arPropinsi[i]).append("\n");
            for (int j = 0; j < arKota[i].length; j++) {
                if (!(i == posProp && j == posKota)) {
                    builder.append(arKota[i][j]).append("\n");
                }
            }
            builder.append("\n");
        }
        tvHasil.setText(builder);
    }
}
