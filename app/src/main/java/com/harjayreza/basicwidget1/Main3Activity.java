package com.harjayreza.basicwidget1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class Main3Activity extends AppCompatActivity {

    /* RadioButton rbBM,rbM,rbD,rbJ;*/
    RadioGroup rgStatus;
    TextView tvHasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        /*rbBM = (RadioButton) findViewById(R.id.RadioButtonBM);
        rbM = (RadioButton) findViewById(R.id.RadioButtonM);
        rbD = (RadioButton) findViewById(R.id.RadioButtonD);
        rbJ = (RadioButton) findViewById(R.id.RadioButtonJ);
*/
        tvHasil = findViewById(R.id.textViewHasil);
        rgStatus = findViewById(R.id.RadioGroupStatus);

        rgStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.RadioButtonBM) {
                    findViewById(R.id.ti1JA).setVisibility(View.GONE);
                } else {
                    findViewById(R.id.ti1JA).setVisibility(View.VISIBLE);
                }
            }
        });

        findViewById(R.id.buttonOK1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doClick();
            }
        });
    }

    private void doClick() {
        String hasil = null;

        if (rgStatus.getCheckedRadioButtonId() != -1) {
            RadioButton rb = findViewById(rgStatus.getCheckedRadioButtonId());
            hasil = rb.getText().toString();

            if (rgStatus.getCheckedRadioButtonId() != R.id.RadioButtonBM) {
                EditText etJA = findViewById(R.id.ti1JA);
                hasil += "\nJumlah Anak: " + etJA.getText();
            }
        }



       /* if (rbBM .isChecked())
        {
            hasil = rbBM.getText().toString();
        }
        else if (rbM.isChecked())
        {
            hasil = rbM.getText().toString();
        }
        else if (rbD.isChecked())
        {
            hasil = rbD.getText().toString();
        }
        else if (rbJ.isChecked())
        {
            hasil = rbJ.getText().toString();
        }*/

        if (hasil == null) {
            tvHasil.setText("Belum Memilih status");
        } else {
            tvHasil.setText("Status Anda : " + hasil);
        }
    }
}
