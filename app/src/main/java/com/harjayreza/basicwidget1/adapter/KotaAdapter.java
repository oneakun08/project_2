package com.harjayreza.basicwidget1.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.harjayreza.basicwidget1.R;

import java.util.ArrayList;

/**
 * Created by Skaha_AM on 19-Jan-18.
 */

public class KotaAdapter extends ArrayAdapter<String> {

    String mPropinsi = "";

    public KotaAdapter(Context context, ArrayList<String> listKota) {
        super(context, R.layout.row_spinner_kota, listKota);
    }

    public void setmPropinsi(String mPropinsi) {
        this.mPropinsi = mPropinsi;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View view, @NonNull ViewGroup parent) {
        return getCostumView(position, view, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View view, @NonNull ViewGroup parent) {
        return getCostumView(position, view, parent);
    }

    private View getCostumView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.row_spinner_kota, parent, false);
        }

        TextView tvTitle = view.findViewById(R.id.textViewTitle);
        tvTitle.setText(getItem(position).substring(0, 1));
        TextView tvKota = view.findViewById(R.id.textViewKota);
        tvKota.setText(getItem(position));
        TextView tvPropinsi = view.findViewById(R.id.textViewPropinsi);
        tvPropinsi.setText(mPropinsi);
        return view;
    }
}
